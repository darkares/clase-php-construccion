<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of construccion
 *
 * @author User
 */
class construccion 
{
    private $zona;
    private $metroscuad;
    private $ancho;
    private $largo;
    
    public function __construct()
    {
        $this->zona="";
        $this->metroscuad="0";
        $this->ancho="0";
        $this->largo="0";
    }
    public function __destruct()
    {
        
    }
    function getZona() {
        return $this->zona;
    }

    function getMetroscuad() {
        return $this->metroscuad;
    }

    function getAncho() {
        return $this->ancho;
    }

    function getLargo() {
        return $this->largo;
    }

    function setZona($zona) {
        $this->zona = $zona;
    }

    function setMetroscuad($metroscuad) {
        $this->metroscuad = $metroscuad;
    }

    function setAncho($ancho) {
        $this->ancho = $ancho;
    }

    function setLargo($largo) {
        $this->largo = $largo;
    }
    
    public function area($ancho,$largo)
    {
        $this->metroscuad= $largo*$ancho;
    }
 }
