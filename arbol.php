<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of arbol
 *
 * @author User
 */
class Arbol 
{
    //atributos de la clase
    private $nombre;
    private $familia;
    private $color;
    private $altura;
    private $radio;
    private $profundidad;
    private $edad;
    private $habitad;
    private $estacion;
    
     //metodos de la clase
    public function __construct()
    {
        $this->nombre="";
        $this->familia="";
        $this->color="verde";        
        $this->altura="0";
        $this->radio="0";
        $this->profundidad="0";
        $this->edad="0";        
        $this->habitad=""; 
        $this->estacion=""; 
    }
    
    public function __destruct() 
    {
        
    }
    
    function getNombre() {
        return $this->nombre;
    }

    function getFamilia() {
        return $this->familia;
    }

    function getColor() {
        return $this->color;
    }

    function getAltura() {
        return $this->altura;
    }

    function getRadio() {
        return $this->radio;
    }

    function getProfundidad() {
        return $this->profundidad;
    }

    function getEdad() {
        return $this->edad;
    }

    function getHabitad() {
        return $this->habitad;
    }
    function getEstacion() {
        return $this->estacion;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setFamilia($familia) {
        $this->familia = $familia;
    }

    function setColor($color) {
        $this->color = $color;
    }

    function setAltura($altura) {
        $this->altura = $altura;
    }

    function setRadio($radio) {
        $this->radio = $radio;
    }

    function setProfundidad($profundidad) {
        $this->profundidad = $profundidad;
    }

    function setEdad($edad) {
        $this->edad = $edad;
    }

    function setHabitad($habitad) {
        $this->habitad = $habitad;
    }
    
    
    function setEstacion($estacion) {
        $this->estacion = $estacion;
    }

        
    public function  actializaredad()
    {
        $this->edad ++;
        
    }
    
     public function  estadoarbol($estacion)
    {
         switch ($estacion){
             case "verano":
                 $this->estacion ="verde";
                 break;
              case "otoño":
                 $this->estacion ="amarillo";
                 break;
              case "invierno":
                 $this->estacion ="sin hojas";
                 break;
              case "primavera":
                 $this->estacion ="verde claso";
                 break;    
        }

    }
}
    

//metodos de la clase

 
